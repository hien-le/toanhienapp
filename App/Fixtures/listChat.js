// Images
import { Images } from '../Themes'

const listChat = [
  {
    name: 'Mary Jane',
    latestChat: 'Thank you so much!',
    timeAgo: 23,
    newMessage: 3,
    avatar: Images.ava1,
    isRead: false,
    isActive: true
  },
  {
    name: 'Rowan Atkinson',
    latestChat: "You know, I've had some beers just sitting in my fridge.",
    timeAgo: 45,
    newMessage: 1,
    avatar: Images.ava2,
    isRead: false,
    isActive: true
  },
  {
    name: 'Daniel Craig',
    latestChat: 'Ok, see u then.',
    timeAgo: 60,
    time: '13:05AM',
    newMessage: 0,
    avatar: Images.ava3,
    isRead: true,
    isActive: false
  },
  {
    name: 'Joaquin Phoenix',
    latestChat: 'Knock knock!',
    timeAgo: 60,
    time: '11:05AM',
    newMessage: 0,
    avatar: Images.ava4,
    isRead: true,
    isActive: false
  },
  {
    name: 'Henry Cavill',
    latestChat: 'Hmmmmm!',
    timeAgo: 60,
    time: '10:45AM',
    newMessage: 0,
    avatar: Images.ava5,
    isRead: true,
    isActive: false
  },
  {
    name: 'Fat Batman',
    latestChat: 'Why did you say that name!!!!',
    timeAgo: 60,
    time: '09:05AM',
    newMessage: 0,
    avatar: Images.ava6,
    isRead: true,
    isActive: false
  },
  {
    name: 'Gal Gadot',
    latestChat: 'Welcome to the land of super girls',
    timeAgo: 60,
    time: '08:05AM',
    newMessage: 0,
    avatar: Images.ava7,
    isRead: true,
    isActive: false
  },
  {
    name: 'Benedict Cumberbatch',
    latestChat: 'I come to bargainnnn!',
    timeAgo: 60,
    time: '08:00AM',
    newMessage: 0,
    avatar: Images.ava8,
    isRead: true,
    isActive: false
  },
  {
    name: 'Mary Jane',
    latestChat: 'Thank you so much!',
    timeAgo: 23,
    newMessage: 3,
    avatar: Images.ava1,
    isRead: false,
    isActive: true
  },
  {
    name: 'Rowan Atkinson',
    latestChat: "You know, I've had some beers just sitting in my fridge.",
    timeAgo: 45,
    newMessage: 1,
    avatar: Images.ava2,
    isRead: false,
    isActive: true
  },
  {
    name: 'Daniel Craig',
    latestChat: 'Ok, see u then.',
    timeAgo: 60,
    time: '13:05AM',
    newMessage: 0,
    avatar: Images.ava3,
    isRead: true,
    isActive: false
  },
  {
    name: 'Joaquin Phoenix',
    latestChat: 'Knock knock!',
    timeAgo: 60,
    time: '11:05AM',
    newMessage: 0,
    avatar: Images.ava4,
    isRead: true,
    isActive: false
  },
  {
    name: 'Henry Cavill',
    latestChat: 'Hmmmmm!',
    timeAgo: 60,
    time: '10:45AM',
    newMessage: 0,
    avatar: Images.ava5,
    isRead: true,
    isActive: false
  },
  {
    name: 'Fat Batman',
    latestChat: 'Why did you say that name!!!!',
    timeAgo: 60,
    time: '09:05AM',
    newMessage: 0,
    avatar: Images.ava6,
    isRead: true,
    isActive: false
  },
  {
    name: 'Gal Gadot',
    latestChat: 'Welcome to the land of super girls',
    timeAgo: 60,
    time: '08:05AM',
    newMessage: 0,
    avatar: Images.ava7,
    isRead: true,
    isActive: false
  },
  {
    name: 'Benedict Cumberbatch',
    latestChat: 'I come to bargainnnn!',
    timeAgo: 60,
    time: '08:00AM',
    newMessage: 0,
    avatar: Images.ava8,
    isRead: true,
    isActive: false
  },
  {
    name: 'Mary Jane',
    latestChat: 'Thank you so much!',
    timeAgo: 23,
    newMessage: 3,
    avatar: Images.ava1,
    isRead: false,
    isActive: true
  },
  {
    name: 'Rowan Atkinson',
    latestChat: "You know, I've had some beers just sitting in my fridge.",
    timeAgo: 45,
    newMessage: 1,
    avatar: Images.ava2,
    isRead: false,
    isActive: true
  },
  {
    name: 'Daniel Craig',
    latestChat: 'Ok, see u then.',
    timeAgo: 60,
    time: '13:05AM',
    newMessage: 0,
    avatar: Images.ava3,
    isRead: true,
    isActive: false
  },
  {
    name: 'Joaquin Phoenix',
    latestChat: 'Knock knock!',
    timeAgo: 60,
    time: '11:05AM',
    newMessage: 0,
    avatar: Images.ava4,
    isRead: true,
    isActive: false
  },
  {
    name: 'Henry Cavill',
    latestChat: 'Hmmmmm!',
    timeAgo: 60,
    time: '10:45AM',
    newMessage: 0,
    avatar: Images.ava5,
    isRead: true,
    isActive: false
  },
  {
    name: 'Fat Batman',
    latestChat: 'Why did you say that name!!!!',
    timeAgo: 60,
    time: '09:05AM',
    newMessage: 0,
    avatar: Images.ava6,
    isRead: true,
    isActive: false
  },
  {
    name: 'Gal Gadot',
    latestChat: 'Welcome to the land of super girls',
    timeAgo: 60,
    time: '08:05AM',
    newMessage: 0,
    avatar: Images.ava7,
    isRead: true,
    isActive: false
  },
  {
    name: 'Benedict Cumberbatch',
    latestChat: 'I come to bargainnnn!',
    timeAgo: 60,
    time: '08:00AM',
    newMessage: 0,
    avatar: Images.ava8,
    isRead: true,
    isActive: false
  }
]

export default listChat
