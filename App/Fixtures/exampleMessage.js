const exampleMessage = [
  {
    _id: 5,
    text: 'Yeah!',
    createdAt: new Date().getTime(),
    user: {
      _id: 1
    }
  },
  {
    _id: 4,
    text: "It's beautiful!",
    createdAt: new Date().getTime(),
    user: {
      _id: 2,
      name: 'Long Tran'
    }
  },
  {
    _id: 3,
    image:
      'https://cdn.cichic.com/media/catalog/product/cache/1/image/5e06319eda06f020e43594a9c230972d/1/0/10107510597-1/white-patchwork-cut-out-v-neck-fashion-maxi-dress.jpg',
    createdAt: new Date().getTime(),
    user: {
      _id: 1
    }
  },
  {
    _id: 1,
    text: 'Nothing speacial actually... How about this?',
    createdAt: new Date().getTime(),
    user: {
      _id: 1
    }
  },
  {
    _id: 2,
    text: 'Hey hey, what are you dressing tonight?',
    createdAt: new Date().getTime(),
    user: {
      _id: 2,
      name: 'Long Tran'
    }
  }
]

export default exampleMessage
