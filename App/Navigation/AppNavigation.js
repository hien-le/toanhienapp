import React from 'react'

// Modules
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'

// Screens
import ListChatScreen from '../Containers/ListChatScreen/ListChatScreen' //import index tu 1 container
import ChatRoomScreen from '../Containers/ChatRoomScreen/ChatRoomScreen'

const AppNavigation = () => {
  const Stack = createStackNavigator()
  return (
    <NavigationContainer initialRouteName={'MainCategoryScreen'}>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name={'ListChatScreen'} component={ListChatScreen} />
        <Stack.Screen name={'ChatRoomScreen'} component={ChatRoomScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default AppNavigation
