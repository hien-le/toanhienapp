import React, { useState } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  StatusBar
} from 'react-native'

// Data
import { listChat } from '../../Fixtures'

// Navigation
import { useNavigation } from '@react-navigation/native'

// Images
import { Images } from '../../Themes'

// Styles
import styles from './Styles/ListChatStyle'

const renderKeyExtractor = (item, index) => {
  return 'keyChat' + index
}

const ListChatScreen = () => {
  const navigation = useNavigation()

  const [page, setPage] = useState(0)
  const [dataChat, setDataChat] = useState(listChat)

  const renderItemSeparator = () => {
    return <View style={styles.separator} />
  }

  const renderListChat = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('ChatRoomScreen')}
        style={styles.chatCard}
      >
        <Image style={styles.avaChat} source={item.avatar} />
        <View style={styles.informationContainer}>
          <Text numberOfLines={1} style={styles.userName}>
            {item.name}
          </Text>
          {item.isRead ? (
            <Text numberOfLines={2} style={styles.chatLine}>
              {item.latestChat}
            </Text>
          ) : (
            <Text numberOfLines={2} style={styles.chatLineNotRead}>
              {item.latestChat}
            </Text>
          )}
        </View>
        <View style={styles.timeContainer}>
          {item.timeAgo < 60 ? (
            <Text numberOfLines={1} style={styles.timeAgo}>
              {item.timeAgo} mins
            </Text>
          ) : (
            <Text numberOfLines={1} style={styles.timeAgo}>
              {item.time}
            </Text>
          )}
          {item.newMessage > 0 && (
            <View style={styles.newMessContainer}>
              <Text numberOfLines={1} style={styles.newMessText}>
                {item.newMessage}
              </Text>
            </View>
          )}
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <View style={styles.container}>
      <StatusBar
        translucent
        backgroundColor='transparent'
        barStyle={'dark-content'}
      />
      <View style={styles.topButtonContainer}>
        <TouchableOpacity style={styles.plusContainer}>
          <Image
            style={styles.plusButton}
            resizeMode={'contain'}
            source={Images.plus}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.categoryContainer}>
          <Image
            style={styles.categoryButton}
            resizeMode={'contain'}
            source={Images.threedot}
          />
        </TouchableOpacity>
      </View>
      <Text numberOfLines={1} style={styles.messageTitle}>
        Messages
      </Text>
      <FlatList
        keyExtractor={renderKeyExtractor}
        style={styles.listChat}
        data={listChat}
        ItemSeparatorComponent={renderItemSeparator}
        renderItem={renderListChat}
        initialNumToRender={10}
      />
    </View>
  )
}
export default ListChatScreen
