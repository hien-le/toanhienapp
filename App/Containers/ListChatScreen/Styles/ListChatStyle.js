import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  topButtonContainer: {
    alignSelf: 'flex-end',
    marginRight: Metrics.doubleBaseMargin,
    marginTop: Metrics.icons.large,
    flexDirection: 'row'
  },
  plusContainer: {
    height: Metrics.icons.medium,
    width: Metrics.icons.medium,
    alignItems: 'center',
    justifyContent: 'center'
  },
  plusButton: {
    height: Metrics.icons.small,
    width: Metrics.icons.small,
    tintColor: Colors.sorrellBrown
  },
  separator: {
    height: Metrics.horizontalLineHeight,
    backgroundColor: Colors.cloud,
    width: Metrics.screenWidth / 1.4,
    alignSelf: 'center',
    marginLeft: Metrics.doubleSection
  },
  categoryContainer: {
    height: Metrics.icons.medium,
    width: Metrics.icons.medium,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: Metrics.doubleBaseMargin
  },
  categoryButton: {
    height: Metrics.icons.medium,
    width: Metrics.icons.medium,
    tintColor: Colors.sorrellBrown
  },
  messageTitle: {
    fontSize: Metrics.icons.large,
    fontWeight: 'bold',
    marginLeft: Metrics.doubleBaseMargin,
    marginBottom: Metrics.doubleBaseMargin
  },
  //listChat: {
  //  backgroundColor: 'yellow'
  //}
  chatCard: {
    height: Metrics.screenHeight / 11,
    marginBottom: Metrics.baseMargin,
    flexDirection: 'row',
    marginTop: Metrics.baseMargin
  },
  avaChat: {
    height: Metrics.images.large,
    width: Metrics.images.large,
    alignSelf: 'center',
    marginLeft: Metrics.doubleBaseMargin,
    borderRadius: 100
  },
  informationContainer: {
    marginLeft: Metrics.baseMargin,
    marginTop: Metrics.baseMargin
  },
  userName: {
    fontSize: Fonts.size.regular,
    color: Colors.sorrellBrown
  },
  chatLine: {
    width: Metrics.screenWidth / 1.6
  },
  chatLineNotRead: {
    fontWeight: 'bold',
    width: Metrics.screenWidth / 1.6
  },
  timeContainer: {
    marginLeft: Metrics.baseMargin,
    marginTop: Metrics.baseMargin
  },
  timeAgo: {
    color: Colors.charcoal,
    fontSize: Fonts.size.small
  },
  newMessContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.newYorkPink,
    height: Metrics.doubleBaseMargin,
    width: Metrics.doubleBaseMargin,
    alignSelf: 'center',
    borderRadius: 100,
    marginTop: Metrics.baseMargin
  },
  newMessText: {
    color: Colors.white
  }
})
