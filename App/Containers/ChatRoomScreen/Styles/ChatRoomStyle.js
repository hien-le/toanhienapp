import { StyleSheet } from 'react-native'
import { Metrics, Colors, Fonts } from '../../../Themes'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  headerContainer: {
    height: Metrics.screenHeight / 12,
    marginTop: Metrics.icons.medium,
    borderBottomColor: Colors.silver,
    borderBottomWidth: Metrics.horizontalLineHeight,
    flexDirection: 'row'
  },
  avatar: {
    height: Metrics.icons.large,
    width: Metrics.icons.large,
    borderRadius: 100,
    alignSelf: 'center',
    marginLeft: Metrics.baseMargin
  },
  userName: {
    fontSize: Fonts.size.h5,
    fontWeight: 'bold',
    marginLeft: Metrics.baseMargin,
    alignSelf: 'center'
  },
  phoneIcon: {
    height: Metrics.icons.medium,
    width: Metrics.icons.medium,
    alignSelf: 'center',
    marginLeft: Metrics.screenWidth / 3,
    tintColor: Colors.newYorkPink
  },
  searchIcon: {
    height: Metrics.images.large,
    width: Metrics.images.large,
    alignSelf: 'center',
    tintColor: Colors.newYorkPink,
    marginBottom: Metrics.smallMargin,
    marginRight: Metrics.smallMargin
  },
  backButton: {
    height: Metrics.icons.medium,
    width: Metrics.icons.medium,
    marginTop: Metrics.icons.small,
    marginLeft: Metrics.baseMargin,
    tintColor: Colors.newYorkPink
  },
  sendingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginRight: Metrics.baseMargin,
    marginBottom: Metrics.icons.tiny
  },
  sendButton: {
    height: Metrics.icons.small,
    width: Metrics.icons.small,
    tintColor: Colors.newYorkPink
  },
  wrapRight: {
    backgroundColor: '#DB8888'
  },
  wrapLeft: {
    backgroundColor: '#E6E6E6'
  },
  textRight: {
    color: '#fff',
    marginTop: 10,
    marginBottom: 10
  },
  textLeft: {
    color: 'black',
    marginTop: 10,
    marginBottom: 10
  }
})
