import React, { useState } from 'react'
import { View, Text, Image, TouchableOpacity, StatusBar } from 'react-native'
import { GiftedChat, Bubble, Send } from 'react-native-gifted-chat'

// Data
import { exampleMessage } from '../../Fixtures'

// Navigation
import { useNavigation } from '@react-navigation/native'

// Images
import { Images } from '../../Themes'

// Styles
import styles from './Styles/ChatRoomStyle'

const ChatRoomScreen = () => {
  const navigation = useNavigation()

  const [messages, setMessages] = useState(exampleMessage)

  function renderBubble(props) {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          right: styles.wrapRight,
          left: styles.wrapLeft
        }}
        textStyle={{
          right: styles.textRight,
          left: styles.textLeft
        }}
      />
    )
  }

  function renderSend(props) {
    return (
      <Send alwaysShowSend={'true'} {...props}>
        <View style={styles.sendingContainer}>
          <Image
            style={styles.sendButton}
            resizeMode={'contain'}
            source={Images.send}
          />
        </View>
      </Send>
    )
  }

  function handleSend(newMessage = []) {
    setMessages(GiftedChat.append(messages, newMessage))
  }

  const onPressGoBack = () => {
    navigation.goBack()
  }

  return (
    <View style={styles.container}>
      <StatusBar
        translucent
        backgroundColor='transparent'
        barStyle={'dark-content'}
      />
      <View style={styles.headerContainer}>
        <TouchableOpacity onPress={onPressGoBack}>
          <Image style={styles.backButton} source={Images.back} />
        </TouchableOpacity>
        <Image style={styles.avatar} source={Images.ava1} />
        <Text numberOfLines={1} style={styles.userName}>
          Kelly West
        </Text>
        <Image
          resizeMode={'contain'}
          style={styles.phoneIcon}
          source={Images.phone}
        />
        <Image
          resizeMode={'contain'}
          style={styles.searchIcon}
          source={Images.search}
        />
      </View>

      <GiftedChat
        messages={messages}
        onSend={(newMessage) => handleSend(newMessage)}
        renderAvatarOnTop={true}
        placeholder={'Type something'}
        renderBubble={renderBubble}
        renderSend={renderSend}
        user={{ _id: 1 }}
      />
    </View>
  )
}
export default ChatRoomScreen
