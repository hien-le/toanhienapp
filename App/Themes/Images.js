const images = {
  category: require('../Images/category.svg'),
  plus: require('../Images/plus.png'),
  threedot: require('../Images/threedot.png'),
  ava1: require('../Images/Avatar/1.jpg'),
  ava2: require('../Images/Avatar/2.jpg'),
  ava3: require('../Images/Avatar/3.jpg'),
  ava4: require('../Images/Avatar/4.jpeg'),
  ava5: require('../Images/Avatar/5.jpg'),
  ava6: require('../Images/Avatar/6.jpg'),
  ava7: require('../Images/Avatar/7.jpg'),
  ava8: require('../Images/Avatar/8.jpg'),
  send: require('../Images/send.png'),
  back: require('../Images/back.png'),
  phone: require('../Images/phone.png'),
  search: require('../Images/search.png'),
  dress: require('../Images/dress.jpg')
}

export default images
